<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request,Collection,DB;
use Illuminate\Support\Facades\Input;
use App\Exports\MetersDetailsExport;
use Maatwebsite\Excel\Facades\Excel;

class MeterController extends Controller
{

  private $meter_no=0;


   function __construct() {

    $this->oracle_connect=DB::connection('oracle');
        
    }


  public function all(Request $request)
  {


 
    //$users = DB::connection('oracle')->table('METER_BASE')->select('*')->take(1)->get();
    // dd($users->toArray());

/*dd($this->get_meter_type('2980128')->METERTYPE);*/

   $meter_number      =($request->meter_number=='')?'meter_details.meter_number':$request->meter_number;
   $meter_type        =($request->meter_type=='')?'meter_details.meter_type':"'".$request->meter_type."'";
   $meter_mark        =($request->meter_mark=='')?'meter_details.meter_mark':"'%".$request->meter_mark."%'";
   $vm                =($request->vm=='')?'meter_details.vm':$request->vm;
   $vm_consumption    =($request->vm_consumption=='')?'meter_details.vm_consumption':$request->vm_consumption;
   $vbt               =($request->vbt=='')?'meter_details.vbt':$request->vbt;
   $vbt_consumption   =($request->vbt_consumption=='')?'meter_details.vbt_consumption':$request->vbt_consumption;
   $vbt_vm_difference =($request->vbt_vm_difference=='')?'meter_details.vbt_vm_difference':$request->vbt_vm_difference;
   $zeit              =($request->zeit=='')?'meter_details.zeit':"'".$request->zeit."'";
   $archive_from_date  =($request->archive_from_date=='')?'meter_details.zeit':"'".$request->archive_from_date."'";
   $archive_to_date    =($request->archive_to_date=='')?'meter_details.zeit':"'".$request->archive_to_date."'";
   $created_from_date  =($request->created_from_date=='')?'meter_details.created_at':"'".$request->created_from_date."'";
   $created_to_date    =($request->created_to_date=='')?'meter_details.created_at':"'".$request->created_to_date."'";



   $result=\App\MeterDetails::whereRaw("meter_details.meter_number = $meter_number")
   ->whereRaw("meter_details.meter_mark LIKE $meter_mark")
   ->whereRaw("meter_details.vm=$vm")
   ->whereRaw("meter_details.vm_consumption=$vm_consumption")
   ->whereRaw("meter_details.vbt=$vbt")
   ->whereRaw("meter_details.vbt_consumption=$vbt_consumption")
   ->whereRaw("meter_details.vbt_vm_difference=$vbt_vm_difference")
   ->whereRaw("meter_details.meter_type=$meter_type")
   ->whereRaw("meter_details.zeit>=$archive_from_date")
   ->whereRaw("meter_details.zeit<=$archive_to_date")
   ->whereRaw("meter_details.created_at>=$created_from_date")
   ->whereRaw("meter_details.created_at<=$created_to_date");





   if(!empty($request->problems))
   {

    $problems=$request->problems;

      $this->multiple_select($result,$problems);
    }





  $meters=$result->orderBy('id','ASC')->paginate(30);
  return response()->json(["meters"=>$meters]);

}

public function multiple_select($result,$problems)

{

$result->where(function($query) use ($problems,$result)
    {
      $result=$query;

        $this->whereCondition('vm_consumption',$problems,$query,"<0");
        $this->whereCondition('vbt_consumption',$problems,$query,"<0");
        $this->whereCondition('vbt_vm_difference',$problems,$query,"<0");
        $this->whereCondition('vm_vbt_proportionality',$problems,$query,"='Problem'");
        $this->whereCondition('vm_consumption_bounce',$problems,$query,"='Problem'");
        $this->whereCondition('vbt_consumption_bounce',$problems,$query,"='Problem'");
        $this->whereCondition('pmp_stuck',$problems,$query,"='Problem'");
        $this->whereCondition('tmp_stuck',$problems,$query,"='Problem'");
        $this->whereCondition('vb_vbt_proportionality',$problems,$query,"='Problem'");
        $this->whereCondition('vm_vmt_proportionality',$problems,$query,"='Problem'");
        $this->whereCondition('pmp_cmp_difference',$problems,$query,"='Problem'");
        $this->whereCondition('vm_qmax_comparison',$problems,$query,"='Problem'");
        $this->whereCondition('vm_qmin_comparison',$problems,$query,"='Problem'");
        $this->whereCondition('indicator_relativity',$problems,$query,"='Problem'");
 










      /*if(in_array("vm_consumption", $request->problems) && array_search('vm_consumption',$request->problems)==0)
      {

        $result->whereRaw("meter_details.vm_consumption <0");
      }

      if( in_array("vm_consumption", $request->problems) &&  array_search('vm_consumption',$request->problems)!=0)
      {

       $result->orWhereRaw("meter_details.vm_consumption <0");
     }



     if( in_array("vbt_consumption", $request->problems) &&  array_search('vbt_consumption',$request->problems)==0){


      $result->whereRaw("meter_details.vbt_consumption <0");


    }

    if( in_array("vbt_consumption", $request->problems) &&  array_search('vbt_consumption',$request->problems)!=0){

     $result->orWhereRaw("meter_details.vbt_consumption <0");


   }*/












});

}


public function whereCondition($field,$problems,$result,$where)
{
   if( in_array($field, $problems) &&  array_search($field,$problems)==0){


    $result->whereRaw("meter_details.".$field.$where);


  }

  if( in_array($field, $problems) &&  array_search($field,$problems)!=0){

   $result->orWhereRaw("meter_details.".$field.$where);


 }
}

public function get($id)
{

  $meter=\App\MeterDetails::whereId($id)->first();
  return response()->json(["meter"=>$meter],200);

}

public function new(\App\Http\Requests\CreateCustomerRequest $request)
{
  $customer=\App\Customer::create($request->only(["name","email","phone","website"]));

  return response()->json(["customer"=>$customer],200);
}

public function upload(Request $request)
{

  $array=$this->convert_to_array($request);
  $meter_no=$this->strafter($this->meter_no,'=');
  $meter_det_prev=\App\MeterDetails::where('meter_number',$meter_no)->orderBy('id','DESC')->first();
  $get_four_records=\App\MeterDetails::where('meter_number',$meter_no)->orderBy('id','DESC')->get()->take(4);

 



  $first_vb_current=$array[1][6];
  $first_vbt_current=$array[1][8];
  $first_vm_current=$array[1][4];
  $first_vmt_current=$array[1][2];
  $first_pmp_current=$array[1][10];
  $first_cmp_current=$array[1][16];
  $first_tmp_current=$array[1][12];
  $first_vm_consumption=($meter_det_prev!=null)?$first_vm_current-$meter_det_prev->vm:'0';
  $first_vbt_consumption=($meter_det_prev!=null)?$first_vbt_current-$meter_det_prev->vbt:'0';
  $first_vbt_vm_difference=$first_vbt_current-$first_vm_current;

  $meter_type=$this->get_meter_type($meter_no);
 //   echo $meter_det_prev->vm;

  $first_vm_consumption_bounce=(($meter_det_prev!=null) && ($meter_det_prev->vm!=0 && ($first_vm_current > $meter_det_prev->vm*5)))?'Problem':''; 
  $first_vbt_consumption_bounce=(($meter_det_prev!=null) && ($meter_det_prev->vbt!=0 && ($first_vbt_current > $meter_det_prev->vbt*5)))?'Problem':''; 
  $qmax=$meter_type->Qmak;
  $qmin=$meter_type->Qmin;
   //  echo $first_vm_current." ".$meter_det_prev->vm*5;
    // echo $first_vm_consumption_bounce;

/*    $first_vbt_vm_difference=($meter_det_prev!=null)? 
    $first_vbt_consumption_bounce=($meter_det_prev!=null)? 
*/

/* if($meter_det_prev!=null)
 {

    echo $first_vm_current.' - '.$meter_det_prev->vm;

  }*/

  //  echo     $first_vm_consumption;
//echo "PMP ".$get_four_records->count()." | ".$first_pmp_current." | ".$get_four_records[0]->pmp." | ".$get_four_records[1]->pmp." | ".$get_four_records[2]->pmp." | ".$get_four_records[3]->pmp;
  $first_vb_vbt_proportionality=($first_vb_current!=$first_vbt_current)?'Problem':'';
  $first_vm_vmt_proportionality=($first_vm_current!=$first_vmt_current)?'Problem':'';
    $first_pmp_cmp_difference= (($first_pmp_current-$first_cmp_current)>1 || ($first_pmp_current-$first_cmp_current)<-1)?'Problem':'';//+;
    

    $first_pmp_stuck=(($get_four_records->count()==4) && ($first_pmp_current==$get_four_records[0]->pmp) && ($get_four_records[0]->pmp==$get_four_records[1]->pmp) && ($get_four_records[1]->pmp==$get_four_records[2]->pmp) && ($get_four_records[2]->pmp==$get_four_records[3]->pmp))?'Problem':'';
    $first_tmp_stuck=(($get_four_records->count()==4) && ($first_tmp_current==$get_four_records[0]->tmp) && ($get_four_records[0]->tmp==$get_four_records[1]->tmp) && ($get_four_records[1]->tmp==$get_four_records[2]->tmp) && ($get_four_records[2]->tmp==$get_four_records[3]->tmp))?'Problem':'';
    
  //  echo $get_four_records->count()." | ".$first_pmp_current." | ".$get_four_records[0]->pmp." | ".$get_four_records[1]->pmp." | ".$get_four_records[2]->pmp." | ".$get_four_records[3]->pmp;
    
 //  echo $meter_no;
    

    $first_vm_consumption=($meter_det_prev!=null)?$first_vm_current-$meter_det_prev->vm:0;

  //  $mtr_num=(string)$meter_no;

  //  dd($this->get_meter_type($mtr_num));

    $insertData=array();
    $insertData[] = array(
      'indicator_id' =>$array[1][0],
      'meter_number'=>$meter_no,
      'vm_consumption'=>$first_vm_consumption,
      'vbt_consumption'=>$first_vbt_consumption,
      'vbt_vm_difference'=>$first_vbt_vm_difference,
      'vm_consumption_bounce'=>$first_vm_consumption_bounce,
      'vbt_consumption_bounce'=>$first_vbt_consumption_bounce,
      'vb_vbt_proportionality'=>$first_vb_vbt_proportionality,
      'vm_vmt_proportionality'=>$first_vm_vmt_proportionality,
      'pmp_cmp_difference'=>$first_pmp_cmp_difference,
      'vm' =>$array[1][4],
      'vbt'=>$array[1][8],
      'zeit' =>date("Y-m-d H:i:s", strtotime($array[1][1])),
      'vmt' =>$array[1][2],
      'vmts' =>$array[1][3],
      'vms' =>$array[1][5],
      'vb' =>$array[1][6],
      'vbs' =>$array[1][7],
      'vbts' =>$array[1][9],
      'pmp' =>$array[1][10],
      'pmps' =>$array[1][11],
      'tmp' =>$array[1][12],
      'tmps' =>$array[1][13],
      'kmp' =>$array[1][14],
      'kmps' =>$array[1][15],
      'cmp' =>$array[1][16],
      'cmps' =>$array[1][17],
      'st2' =>$array[1][18],
      'st4' =>$array[1][19],
      'st7' =>$array[1][20],
      'st6' =>$array[1][21],
      'stsy' =>$array[1][22],
      'ev' =>$array[1][23],
      'gono' =>$array[1][24],
      'check' =>$array[1][25],
      'pmp_stuck'=>$first_pmp_stuck,
      'tmp_stuck'=>$first_tmp_stuck,
      'meter_mark'=>$meter_type->METERTYPE,
      'qmin'=>$qmin,
      'qmax'=>$qmax,
      'vm_qmax_comparison'=>($first_vm_current>$qmax)?'Problem':'',
      'vm_qmin_comparison'=>($first_vm_current<$qmin)?'Problem':'',
      );



//print_r($array);

    foreach($array as $key=>$value)
    {


 //     $pmp1=($key>=5 && $key<=count($array))?$array[$key][10]:0;
      $pmp2=($key>=5 && $key<=count($array))?$array[$key-1][10]:0;
      $pmp3=($key>=5 && $key<=count($array))?$array[$key-2][10]:0;
      $pmp4=($key>=5 && $key<=count($array))?$array[$key-3][10]:0;
      $pmp5=($key>=5 && $key<=count($array))?$array[$key-4][10]:0;


      $tmp2=($key>=5 && $key<=count($array))?$array[$key-1][12]:0;
      $tmp3=($key>=5 && $key<=count($array))?$array[$key-2][12]:0;
      $tmp4=($key>=5 && $key<=count($array))?$array[$key-3][12]:0;
      $tmp5=($key>=5 && $key<=count($array))?$array[$key-4][12]:0;


/*if($key>=5 && $key<=count($array))
{

  echo $array[$key][10]." ".$array[$key-1][10]." ".$array[$key-2][10]." ".$array[$key-3][10]." ".$array[$key-4][10]."<br>";

}*/



if($key>1 ){

  $vm_current=$array[$key--][4];
  $vm_prev=$array[$key++][4];
  $vbt_current=$array[$key--][8];
  $vbt_prev=$array[$key++][8];
  $pmp_current=$value[10];
  $cmp_current=$value[16];
  $tmp_current=$value[12];



        $vm_consumption=$vm_current-$vm_prev; //+
        $vbt_consumption=$vbt_current-$vbt_prev;//+
        $vbt_vm_difference=$vbt_current-$vm_current;//+

        $vm_consumption_bounce=($vm_prev!=0 && ($vm_current>$vm_prev*5))?'Problem':'';//+
        $vbt_consumption_bounce=($vbt_prev!=0 && ($vbt_current>$vbt_prev*5))?'Problem':'';//+
        $vb_vbt_proportionality=($value[6]!=$vbt_current)?'Problem':'';//+
        $vm_vmt_proportionality=($value[2]!=$vm_current)?'Problem':'';//+
        $pmp_cmp_difference=(($pmp_current-$cmp_current)>1 || ($pmp_current-$cmp_current)<-1)?'Problem':'';//+

        $pmp_stuck=(($pmp_current==$pmp2) && ($pmp2==$pmp3) && ($pmp3==$pmp4) && ($pmp4==$pmp5))?"Problem":'';
        $tmp_stuck=(($tmp_current==$tmp2) && ($tmp2==$tmp3) && ($tmp3==$tmp4) && ($tmp4==$tmp5))?"Problem":'';
        
//       echo $pmp_current." ".$pmp1." ".$pmp2." ".$pmp3." ".$pmp4." ".$pmp5."<br>";

        $insertData[]= array(
          'indicator_id' =>$value[0],
          'meter_number'=>$meter_no,
          'vm_consumption'=>$vm_consumption,
          'vbt_consumption'=>$vbt_consumption,
          'vbt_vm_difference'=>$vbt_vm_difference,
          'vm_consumption_bounce'=>$vm_consumption_bounce,
          'vbt_consumption_bounce'=>$vbt_consumption_bounce,
          'vb_vbt_proportionality'=>$vb_vbt_proportionality,
          'vm_vmt_proportionality'=>$vm_vmt_proportionality,
          'pmp_cmp_difference'=>$pmp_cmp_difference,
          'vm' =>$vm_current,
          'vbt'=>$vbt_current,
          'zeit' =>date("Y-m-d H:i:s", strtotime($value[1])),
          'vmt' =>$value[2],
          'vmts' =>$value[3],
          'vms' =>$value[5],
          'vb' =>$value[6],
          'vbs' =>$value[7],
          'vbts' =>$value[9],
          'pmp' =>$value[10],
          'pmps' =>$value[11],
          'tmp' =>$value[12],
          'tmps' =>$value[13],
          'kmp' =>$value[14],
          'kmps' =>$value[15],
          'cmp' =>$value[16],
          'cmps' =>$value[17],
          'st2' =>$value[18],
          'st4' =>$value[19],
          'st7' =>$value[20],
          'st6' =>$value[21],
          'stsy' =>$value[22],
          'ev' =>$value[23],
          'gono' =>$value[24],
          'check' =>$value[25],
          'pmp_stuck'=>$pmp_stuck,
          'tmp_stuck'=>$tmp_stuck,
          'meter_mark'=>$meter_type->METERTYPE,
          'qmin'=>$qmin,
          'qmax'=>$qmax,
          'vm_qmax_comparison'=>($first_vm_current>$qmax)?'Problem':'',
          'vm_qmin_comparison'=>($first_vm_current<$qmin)?'Problem':'',
          );
      }






    }

           //   print_r($insertData);

    $insert_data = collect($insertData);
    $chunks = $insert_data->chunk(10);

    foreach ($chunks as $chunk)
    {
        //    print_r($chunk->toArray());


      \App\MeterDetails::insertIgnore($chunk->toArray());

    }


  }


  private function convert_to_array($request)
  {
   $filepath=$request->file->path();
   $array = array();
   if (!file_exists($filepath)){ return $array; }
   $content = file($filepath);
   $i=1;

   //    echo $content[13];
   $this->meter_no=$content[13];
   for ($x=0; $x < count($content); $x++){

           if (trim($content[$x]) != '' && $x>=110){//gets column values aferline 110
             $line = explode("\t", trim($content[$x]));

             $array[$i++] = $line;
           }
         }

         return $array;
       }


       function strafter($string, $substring) {
         $pos = strpos($string, $substring);
         if ($pos === false)
          return $string;
        else
          return(substr($string, $pos+strlen($substring)));
      }

      public function excel(Request $request)
      {
       $meter_number      =($request->meter_number=='')?'meter_details.meter_number':$request->meter_number;
       $meter_type        =($request->meter_type=='')?'meter_details.meter_type':"'".$request->meter_type."'";
       $meter_mark        =($request->meter_mark=='')?'meter_details.meter_mark':"'".$request->meter_mark."'";
       $vm                =($request->vm=='')?'meter_details.vm':$request->vm;
       $vm_consumption    =($request->vm_consumption=='')?'meter_details.vm_consumption':$request->vm_consumption;
       $vbt               =($request->vbt=='')?'meter_details.vbt':$request->vbt;
       $vbt_consumption   =($request->vbt_consumption=='')?'meter_details.vbt_consumption':$request->vbt_consumption;
       $vbt_vm_difference =($request->vbt_vm_difference=='')?'meter_details.vbt_vm_difference':$request->vbt_vm_difference;
       $zeit              =($request->zeit=='')?'meter_details.zeit':"'".$request->zeit."'";
       $archive_from_date  =($request->archive_from_date=='')?'meter_details.zeit':"'".$request->archive_from_date."'";
       $archive_to_date    =($request->archive_to_date=='')?'meter_details.zeit':"'".$request->archive_to_date."'";
       $created_from_date  =($request->created_from_date=='')?'meter_details.created_at':"'".$request->created_from_date."'";
       $created_to_date    =($request->created_to_date=='')?'meter_details.created_at':"'".$request->created_to_date."'";


       $result=\App\MeterDetails::select('meter_number AS Sayğac nömrəsi',
                                          'meter_type AS Sayğac tipi',
                                          'meter_mark AS Sayğac markası',
                                          'created_at AS Yüklənmə Tarix saat',
                                          'zeit AS Arxiv tarix saatı',
                                          'vm AS Vm',
                                          'vm_consumption AS Vm sərfiyyat',
                                          'vbt AS VbT',
                                          'vbt_consumption AS VbT sərfiyyat',
                                          'vm_vbt_proportionality AS Vm Vbt mütənasibliyi',
                                          'vm_consumption_bounce AS Vm sərfiyyat sıçrayışı',
                                          'vbt_consumption_bounce AS Vbt sərfiyyat sıçrayışı',
                                          'pmp AS PMP',
                                          'pmp_stuck AS P.MP ilişməsi',
                                          'tmp AS TMP',
                                          'tmp_stuck AS T.MP ilişməsi',
                                          'vb AS Vb',
                                          'vb_vbt_proportionality AS Vb Vbt uyğunluğu',
                                          'vmt AS Vmt',
                                          'vm_vmt_proportionality AS Vm Vmt uyğunluğu',
                                          'cmp AS CMP',
                                          'pmp_cmp_difference AS PMP CMP fərqi',
                                          'qmax AS Qmax',
                                          'vm_qmax_comparison AS Vm Qmax müqayisə',
                                          'qmin AS Qmin',
                                          'vm_qmin_comparison AS Vm Qmin müqayisə',
                                          'indicator_relativity AS Göstəricilərin mütanasibliyi',
                                          'indicator_id',
                                          'vbs',
                                          'vbts',
                                          'pmps',
                                          'tmps',
                                          'kmp',
                                          'kmps',
                                          'cmp',
                                          'cmps',
                                          'st2',
                                          'st4',
                                          'st7',
                                          'st6',
                                          'stsy',
                                          'ev',
                                          'gono',
                                          'check'
                                                )
       
       ->whereRaw("meter_details.meter_number = $meter_number")
       ->whereRaw("meter_details.meter_mark =$meter_mark")
       ->whereRaw("meter_details.vm =$vm")
       ->whereRaw("meter_details.vm_consumption =$vm_consumption")
       ->whereRaw("meter_details.vbt =$vbt")
       ->whereRaw("meter_details.vbt_consumption =$vbt_consumption")
       ->whereRaw("meter_details.vbt_vm_difference =$vbt_vm_difference")
       ->whereRaw("meter_details.meter_type =$meter_type")
       ->whereRaw("meter_details.zeit >=$archive_from_date")
       ->whereRaw("meter_details.zeit <=$archive_to_date")
       ->whereRaw("meter_details.created_at >=$created_from_date")
       ->whereRaw("meter_details.created_at <=$created_to_date");


       if(!empty($request->problems))
       {

        $problems=$request->problems;

          $this->multiple_select($result,$problems);
        }

      $meters=$result->orderBy('id','ASC')->get();
      return response()->json(["meters"=>$meters],200);
    }

    private function get_meter_type($meter_number){

      //  DB::connection('oracle')->enableQueryLog();
      // echo 'burada'.$meter_number;
       //$meter_type_id=DB::connection('oracle')->table('METER_BASE')->where('METER_NUMBER',$meter_number)->first()->meter_type_id;
    //  echo $meter_type_id;
       $m2=str_replace(["\t","\r","\n"], "", $meter_number);

        //DB::connection('oracle')->table('AHALI.METER_BASE')->whereRaw("METER_NUMBER = '".$m2."'")->first();

      $meter_type_id=$this->oracle_connect->table('AHALI.METER_BASE')->where('METER_NUMBER',$m2)->first()->meter_type_id;
      $meter_type=\App\MeterType::where('METERID',$meter_type_id)->first();

     // return DB::connection('oracle')->getQueryLog();


     return  $meter_type;

    }

  }
