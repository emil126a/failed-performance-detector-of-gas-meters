<?php

namespace App\Exports;

use App\MeterDetails;
use Maatwebsite\Excel\Concerns\FromCollection;

class MetersDetailsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return MeterDetails::all();
    }
}
