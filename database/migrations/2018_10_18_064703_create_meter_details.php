<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeterDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meter_details', function (Blueprint $table) {
            $table->increments('id');
                      $table->unique(['zeit', 'meter_number']);
                      $table->integer('indicator_id')->nullable($value = true);
                      $table->integer('meter_number');
                      $table->timestamp('zeit')->nullable($value = true);
                      $table->integer('vmt')->nullable($value = true);
                      $table->integer('vmts')->nullable($value = true);
                      $table->integer('vm')->nullable($value = true);



                      $table->integer('vms')->nullable($value = true);
                      $table->double('vb')->nullable($value = true);

                      $table->integer('vbs')->nullable($value = true);
                      $table->double('vbt')->nullable($value = true);
                      $table->integer('vbts')->nullable($value = true);

                      $table->double('pmp')->nullable($value = true);
                      $table->integer('pmps')->nullable($value = true);
                      $table->double('tmp')->nullable($value = true);
                      $table->integer('tmps')->nullable($value = true);
                      $table->double('kmp')->nullable($value = true);
                      $table->integer('kmps')->nullable($value = true);
                      $table->double('cmp')->nullable($value = true);
                      $table->integer('cmps')->nullable($value = true);
                      $table->integer('st2')->nullable($value = true);
                      $table->integer('st4')->nullable($value = true);
                      $table->integer('st7')->nullable($value = true);
                      $table->integer('st6')->nullable($value = true);
                      $table->string('stsy')->nullable($value = true);
                      $table->string('ev')->nullable($value = true);
                      $table->string('gono')->nullable($value = true);
                      $table->string('check')->nullable($value = true);

                      $table->integer('vm_consumption')->nullable($value = true);
                      $table->double('vbt_consumption',13,5)->nullable($value = true);
                      $table->double('vbt_vm_difference',13,3)->nullable($value = true);
                      $table->string('vm_vbt_proportionality')->nullable($value = true);
                      $table->string('vm_consumption_bounce')->nullable($value = true);
                      $table->string('vbt_consumption_bounce')->nullable($value = true);
                      $table->string('pmp_stuck')->nullable($value = true);
                      $table->string('tmp_stuck')->nullable($value = true);
                      $table->string('vb_vbt_proportionality')->nullable($value = true);
                      $table->string('vm_vmt_proportionality')->nullable($value = true);
                      $table->string('pmp_cmp_difference')->nullable($value = true);
                      $table->string('vm_qmax_comparison')->nullable($value = true);
                      $table->string('vm_qmin_comparison')->nullable($value = true);
                      $table->string('indicator_relativity')->nullable($value = true);
                      $table->string('meter_type')->default("")->nullable($value = true);
                      $table->string('meter_mark')->default("")->nullable($value = true);

                      $table->double('qmin')->nullable($value = true);
                      $table->double('qmax')->nullable($value = true);

                      $table->timestamp('updated_at');
                      $table->timestamp('created_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meter_details');
    }
}
