<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetersType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meter_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('METERTYPE');
            $table->integer('METERID');
            $table->double('Qmin')->nullable($value = true);
            $table->double('Qnom')->nullable($value = true);
            $table->double('Qmak')->nullable($value = true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('big_meters');
    }
}
