<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group([
    'prefix' => 'auth'
], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});


Route::group([
    'middleware' => 'jwt.auth'
], function ($router) {

    Route::get('meters', 'MeterController@all');
    Route::get('meters/{id}', 'MeterController@get');

    Route::post('meters/new', 'MeterController@new');
    Route::get('excel', 'MeterController@excel');
    Route::post('upload', 'MeterController@upload');
    Route::get('logout', 'AuthController@signout');
});
