export const login = (state) => {
    state.loading = true;
    state.auth_error = null;
}

export const loginSuccess = (state, payload) => {
    state.auth_error = null;
    state.isLoggedIn = true;
    state.loading = false;


    state.currentUser = Object.assign({}, payload.data.user, {token: payload.data.access_token});
    //  console.log(state.currentUser);
    localStorage.setItem("user", JSON.stringify(state.currentUser));
}

export const loginFailed = (state, payload) => {
    state.loading = false;
    state.auth_error = payload.error;
}

export const logout = (state) => {
    axios.get('api/logout', {
        headers: {
            "Authorization": 'Bearer ' + state.currentUser.token
        }
    }).then((response) => {

    })

    state.isLoggedIn = false;
    localStorage.removeItem("user");
    state.currentUser = null;
}


export const clearAnimation = (state) => {
    state.inAnimationClass = '';
    state.outAnimationClass = '';
}

export const updateCustomers = (state, payload) => {
    state.customers = payload
}


export const updateAnimationClass = (state, payload) => {
    state.inAnimationClass = payload.inAnimationClass;
    state.outAnimationClass = payload.outAnimationClass;
}
