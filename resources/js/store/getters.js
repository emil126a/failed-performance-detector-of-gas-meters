export const isLoading= (state)=> {
    return state.loading
}

export const isLoggedIn=(state)=>{
    return state.isLoggedIn
}
export const currentUser=(state)=>{
    return state.currentUser
}
export const authError=(state)=>{
    return state.auth_error
}

/*
export const customers=(state)=>{
    return state.customers
}
*/

export const getAnimationClass=(state)=>{
    return {
        inAnimationClass:'animated '+state.inAnimationClass,
        outAnimationClass:'animated '+state.outAnimationClass
    }
}
