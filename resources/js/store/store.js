import {getLocalUser} from "../helpers/auth";
import * as actions from './actions'
import  * as mutations from './mutations'
import * as getters from './getters'

const user = getLocalUser();// gets user from local storage


export default {


    state: {
        currentUser: user,//user from local storage
        isLoggedIn: !!user,//boolean if null false else true
        loading: false,
        auth_error: null,
        customers: [],
        inAnimationClass:'fadeIn',
        outAnimationClass:'fadeOut'
    },
    getters: getters,
    mutations:mutations,
    actions:actions,

}
