import Login from './components/auth/Login'
import Register from './components/auth/Register'
import MeterList from './components/dashboard/List';
import NewCustomer from './components/dashboard/New';
import adminHome from './components/admin/dashboard/Main';
import Meter from './components/dashboard/Meter';
import fileUpload from './components/dashboard/fileUpload'

const Home = resolve => {
    require.ensure(['./components/dashboard/Home.vue'], () => {
        resolve(require('./components/dashboard/Home.vue'));
    })
}; //Lazy loading

const MeterMain = resolve => {
    require.ensure(['./components/dashboard/Main'], () => {
        resolve(require('./components/dashboard/Main'));
    })
};//Lazy loading


export const routes = [
    {
        path: '/',
        component: Home,

    },
    {
        path: '/user',
        component: Home,
        meta: {
            requiresAuth: true,
            permission: 'user'
        }
    },

    {
        path: '/admin',
        component: adminHome,
        meta: {
            requiresAuth: true,
            permission: 'admin'
        }
    },
    {
        path: '/login',
        component: Login,
        meta: {
            permission: 'guest'
        }
    },
    /* {
         path: '/register',
         component: Register,
         meta: {
             permission:'guest'
         }
     },*/
    {
        path: '/upload',
        component: fileUpload,
        meta: {
            requiresAuth: true,
            permission: 'admin|user'
        }
    },
    {
        path: '/meters',
        component: MeterMain,
        meta: {
            requiresAuth: true,
            permission: 'admin'
        },
        children: [
            {
                path: '/',
                component: MeterList,
                meta: {
                    permission: 'admin'
                },
            },


            {
                path: ':id',
                component: Meter,
                meta: {
                    permission: 'admin'
                },
            },

        ]
    }


]
