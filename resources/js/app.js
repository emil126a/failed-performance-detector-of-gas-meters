require('./bootstrap');
import Vue from 'vue';
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import {routes} from "./routes";
import StoreData from './store/store'
import MainApp from './components/layouts/MainApp'
import VeeValidate from 'vee-validate';


import {initialize} from "./helpers/general";


Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(VeeValidate, {fieldsBagName: 'formFields'})


const store = new Vuex.Store(StoreData);

const router = new VueRouter({
    routes,
    mode: 'history'
});




initialize(store,router);
/*tok_header(store.getters.currentUser);*/


const app = new Vue({
    el: '#app',
    router,
    store,
    components: {
        MainApp
    }
});
