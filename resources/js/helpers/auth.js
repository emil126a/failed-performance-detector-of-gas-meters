export function login(credentials) {
    return new Promise((res, rej) => {
        axios.post('/api/auth/login', credentials)
            .then(response => {
                res(response)
            })
            .catch(err => {
                rej("Bazada belə email və ya şifrə mövcud deyil")
            })
    })
}

/*export function authErrorClass()
{
    return "animated shake";
}*/

export function getLocalUser() {
    const userStr = localStorage.getItem("user");

    if (!userStr) {
        return null;
    }

    return JSON.parse(userStr);
}
