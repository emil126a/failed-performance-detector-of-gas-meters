export function initialize(store, router) {

    router.beforeEach((to, from, next) => {
        const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
        //const admin_access = to.matched.some(record => record.meta.isAdmin);

        const currentUser = store.state.currentUser;
        //console.log(currentUser.is_admin);

        if (requiresAuth && !currentUser) {
            next('/login');
        }
        else if (to.path == '/login' && currentUser) {

            next('/');
        }
        else {
           // console.log(to.meta.permission)


            if ((typeof to.meta.permission !== 'undefined') && (to.meta.permission.includes("|"))) {
                var admin = to.meta.permission.split("|")[0];
                var user = to.meta.permission.split("|")[1];

               // console.log('1');
                if (admin == 'admin' && user == 'user' && currentUser.is_admin == 1) {
                    console.log('2');
                    next();
                }
                else if (user == 'user' && currentUser.is_admin == 0) {
                    console.log('3');
                    next();
                }
                else if (admin == 'admin' && currentUser.is_admin == 1) {
                    console.log('4');
                    next();
                }
                else {
                    console.log('5');
                    next('/');
                }
            }else
            {
                if (to.meta.permission == 'admin' && currentUser.is_admin == 1) {
                    console.log('6')
                    next();
                }
                else if (to.meta.permission == 'user' && currentUser.is_admin == 0) {
                    console.log('7');
                    next();
                }
                else if (to.meta.permission == 'guest') {
                    console.log('8');
                    next();
                }
                else {
                    console.log('9');
                    if (currentUser.is_admin == 0) {
                        next('/user')
                    } else {
                        next('/admin')
                    }

                }

            }


        }
    });

    axios.interceptors.response.use(null, (error) => {
        if (error.response.status == 401) {
            store.commit('logout');
            router.push('/login');
        }

        return Promise.reject(error);
    });


    /*    console.log(store.getters.currentUser)

        axios.defaults.headers.common["Authorewewaasdadsdseization"] = 'asadsdassadads';*/


}

/*
export function tok_header(user){
    if(user!=null)
        console.log(user.token)


}
*/

